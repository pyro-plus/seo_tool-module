<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleSeoToolAddPagesFields extends Migration
{

    protected $fields = [
        'meta_fb_title'       => [
            'namespace' => 'pages',
            'type'      => 'anomaly.field_type.text',
        ],
        'meta_fb_description' => [
            'namespace' => 'pages',
            'type'      => 'anomaly.field_type.textarea',
        ],
        'meta_fb_image'       => [
            'namespace' => 'pages',
            'type'      => 'anomaly.field_type.file',
            'config'    => [
                'folder' => 2,
            ],
        ],
        'meta_tw_title'       => [
            'namespace' => 'pages',
            'type'      => 'anomaly.field_type.text',
        ],
        'meta_tw_description' => [
            'namespace' => 'pages',
            'type'      => 'anomaly.field_type.textarea',
        ],
        'meta_tw_image'       => [
            'namespace' => 'pages',
            'type'      => 'anomaly.field_type.file',
            'config'    => [
                'folder' => 2,
            ],
        ],
    ];
}
