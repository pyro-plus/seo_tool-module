<?php

use Anomaly\Streams\Platform\Database\Migration\Migration;

class DefrModuleSeoToolAddPagesStream extends Migration
{

    /**
     * The addon fields.
     *
     * @var array
     */
    protected $assignments = [
        'meta_fb_title'       => [
            'translatable' => true,
        ],
        'meta_fb_description' => [
            'translatable' => true,
        ],
        'meta_fb_image' => [],
        'meta_tw_title'       => [
            'translatable' => true,
        ],
        'meta_tw_description' => [
            'translatable' => true,
        ],
        'meta_tw_image' => [],
    ];

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $stream = $this->streams()->findBySlugAndNamespace('pages', 'pages');

        foreach ($this->assignments as $slug => $params)
        {
            $field = $this->fields()->findBySlugAndNamespace($slug, 'pages');

            $this->assignments()->create(array_merge([
                'stream' => $stream,
                'field'  => $field,
            ], $params));
        }
    }
}
