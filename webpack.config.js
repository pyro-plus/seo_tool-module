/*eslint-disable*/

var path = require('path');

module.exports = {
  entry: {
    // vue: './resources/js/src/vue.js',
    serp: './resources/js/src/serp.js',
    settings: './resources/js/src/settings.js',
  },
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'resources/js'),
  },
  resolve: {
    alias: {
      vue: 'vue/dist/vue.js',
    },
  },
  module: {
    loaders: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
      },
      {
        test: /\.css$/,
        loader: 'style!css',
      },
      {
        test: /\.(scss|sass)$/,
        loader: 'style!css!sass',
      },
      {
        test: /\.styl(us)?$/,
        loader: 'style!css!stylus',
      },
      {
        test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)$/,
        loader: 'file?name=assets/[name].[ext]',
      },
    ],
  },
};
