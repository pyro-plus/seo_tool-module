import Vue from 'vue'
// import Dynamic from './Dynamic.vue'

Vue.config.silent = false
Vue.config.devTools = true

new Vue({
  el: '#main',
  // components: { Dynamic },

  data () {
    return {
      include_pages: false,
      include_posts: false,
    }
  },

  created () {
    // console.log(this)
  },

  mounted () {
    let vm = this
    this.$el.querySelectorAll('[name^="include_"][type="checkbox"]').forEach(el => {
      vm.$set(vm, el.name, el.checked)
      el.onchange = function (e) {
        vm.$set(vm, e.target.name, e.target.checked)
      }
    })
  }
})
