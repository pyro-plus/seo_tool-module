import Vue from 'vue'
import App from './App.vue'

Vue.config.silent = false
Vue.config.devTools = true

new Vue({
  el: '#reports-app',
  components: { App },
})
