export default (get_as_float) => {
  var unixtime_ms = (new Date).getTime();
  var sec = Math.floor(unixtime_ms / 1000);
  return get_as_float ? (unixtime_ms / 1000) : (unixtime_ms - (sec * 1000)) /
    1000 + ' ' + sec;
}
