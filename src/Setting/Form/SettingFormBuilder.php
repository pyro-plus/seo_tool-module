<?php namespace Defr\SeoToolModule\Setting\Form;

use Anomaly\PagesModule\Type\TypeModel as PageTypeModel;
use Anomaly\PostsModule\Type\TypeModel as PostTypeModel;
use Anomaly\SettingsModule\Setting\Contract\SettingRepositoryInterface;
use Anomaly\Streams\Platform\Ui\Form\FormBuilder;

class SettingFormBuilder extends FormBuilder
{

    /**
     * Form model is empty
     *
     * @var null
     */
    protected $model = null;

    /**
     * Form fields
     *
     * @var array|string
     */
    protected $fields = [
        'title_separator'        => [
            'bind'         => 'defr.module.seo_tool::settings.title_separator',
            'label'        => 'defr.module.seo_tool::settings.title_separator.name',
            'instructions' => 'defr.module.seo_tool::settings.title_separator.instructions',
            'type'         => 'anomaly.field_type.select',
            'config'       => [
                'mode'    => 'radio',
                'options' => [
                    '★' => '★',
                    '♥' => '♥',
                    '✓' => '✓',
                    '|' => '|',
                    '-' => '-',
                    ':' => ':',
                    '~' => '~',
                    '※' => '※',
                    '⁕' => '⁕',
                    '↑' => '↑',
                    '◊' => '◊',
                ],
            ],
        ],
        'readability_analysis'   => [
            'bind'         => 'defr.module.seo_tool::settings.readability_analysis',
            'label'        => 'defr.module.seo_tool::settings.readability_analysis.name',
            'instructions' => 'defr.module.seo_tool::settings.readability_analysis.instructions',
            'type'         => 'anomaly.field_type.boolean',
        ],
        'keywords_analysis'      => [
            'bind'         => 'defr.module.seo_tool::settings.keywords_analysis',
            'label'        => 'defr.module.seo_tool::settings.keywords_analysis.name',
            'instructions' => 'defr.module.seo_tool::settings.keywords_analysis.instructions',
            'type'         => 'anomaly.field_type.boolean',
        ],
        'include_pages'          => [
            'bind'         => 'defr.module.seo_tool::settings.include_pages',
            'label'        => 'defr.module.seo_tool::settings.include_pages.name',
            'instructions' => 'defr.module.seo_tool::settings.include_pages.instructions',
            'type'         => 'anomaly.field_type.boolean',
            // 'attributes'   => [
            //     'v-model' => '$root.includePages',
            // ],
        ],
        'include_posts'          => [
            'bind'         => 'defr.module.seo_tool::settings.include_posts',
            'label'        => 'defr.module.seo_tool::settings.include_posts.name',
            'instructions' => 'defr.module.seo_tool::settings.include_posts.instructions',
            'type'         => 'anomaly.field_type.boolean',
            // 'attributes'   => [
            //     'v-model' => '$root.includePosts',
            // ],
        ],
        'included_page_types'    => [
            'bind'         => 'defr.module.seo_tool::settings.included_page_types',
            'label'        => 'defr.module.seo_tool::settings.included_page_types.name',
            'instructions' => 'defr.module.seo_tool::settings.included_page_types.instructions',
            'type'         => 'anomaly.field_type.multiple',
            'wrapperView'  => 'module::form/partials/wrapper',
            'config'       => [
                'mode'           => 'tags',
                'related'        => PageTypeModel::class,
                'default_value'  => [],
                'vue_attributes' => [
                    'v-show' => 'include_pages',
                ],
            ],
        ],
        'included_post_types'    => [
            'bind'         => 'defr.module.seo_tool::settings.included_post_types',
            'label'        => 'defr.module.seo_tool::settings.included_post_types.name',
            'instructions' => 'defr.module.seo_tool::settings.included_post_types.instructions',
            'type'         => 'anomaly.field_type.multiple',
            'wrapperView'  => 'module::form/partials/wrapper',
            'config'       => [
                'mode'           => 'tags',
                'related'        => PostTypeModel::class,
                'default_value'  => [],
                'vue_attributes' => [
                    'v-show' => 'include_posts',
                ],
            ],
        ],
        // 'onpage_check'           => [
        //     'bind'         => 'defr.module.seo_tool::settings.onpage_check',
        //     'label'        => 'defr.module.seo_tool::settings.onpage_check.name',
        //     'instructions' => 'defr.module.seo_tool::settings.onpage_check.instructions',
        //     'type'         => 'anomaly.field_type.boolean',
        // ],
        // 'admin_bar_menu'         => [
        //     'bind'         => 'defr.module.seo_tool::settings.admin_bar_menu',
        //     'label'        => 'defr.module.seo_tool::settings.admin_bar_menu.name',
        //     'instructions' => 'defr.module.seo_tool::settings.admin_bar_menu.instructions',
        //     'type'         => 'anomaly.field_type.boolean',
        // ],
        'website_name'           => [
            'bind'         => 'defr.module.seo_tool::settings.website_name',
            'label'        => 'defr.module.seo_tool::settings.website_name.name',
            'instructions' => 'defr.module.seo_tool::settings.website_name.instructions',
            'type'         => 'anomaly.field_type.text',
        ],
        'alternate_name'         => [
            'bind'         => 'defr.module.seo_tool::settings.alternate_name',
            'label'        => 'defr.module.seo_tool::settings.alternate_name.name',
            'instructions' => 'defr.module.seo_tool::settings.alternate_name.instructions',
            'type'         => 'anomaly.field_type.text',
        ],
        'bing_webmaster_tools'   => [
            'bind'         => 'defr.module.seo_tool::settings.bing_webmaster_tools',
            'label'        => 'defr.module.seo_tool::settings.bing_webmaster_tools.name',
            'instructions' => 'defr.module.seo_tool::settings.bing_webmaster_tools.instructions',
            'type'         => 'anomaly.field_type.text',
        ],
        'google_search_console'  => [
            'bind'         => 'defr.module.seo_tool::settings.google_search_console',
            'label'        => 'defr.module.seo_tool::settings.google_search_console.name',
            'instructions' => 'defr.module.seo_tool::settings.google_search_console.instructions',
            'type'         => 'anomaly.field_type.text',
        ],
        'yandex_webmaster_tools' => [
            'bind'         => 'defr.module.seo_tool::settings.yandex_webmaster_tools',
            'label'        => 'defr.module.seo_tool::settings.yandex_webmaster_tools.name',
            'instructions' => 'defr.module.seo_tool::settings.yandex_webmaster_tools.instructions',
            'type'         => 'anomaly.field_type.text',
        ],
        'security_setting'       => [
            'bind'         => 'defr.module.seo_tool::settings.security_setting',
            'label'        => 'defr.module.seo_tool::settings.security_setting.name',
            'instructions' => 'defr.module.seo_tool::settings.security_setting.instructions',
            'type'         => 'anomaly.field_type.boolean',
        ],
    ];

    /**
     * The form sections handler.
     *
     * @var string
     */
    protected $sections = [
        'general' => [
            'tabs' => [
                'dashboard' => [
                    'title' => 'Dashboard',
                ],
                'general'   => [
                    'title'  => 'General',
                    'fields' => [
                        'title_separator',
                        'readability_analysis',
                        'keywords_analysis',
                    ],
                ],
                'features'  => [
                    'title'  => 'Includes',
                    'fields' => [
                        'include_pages',
                        'included_page_types',
                        'include_posts',
                        'included_post_types',
                    ],
                ],
                'info'      => [
                    'title'  => 'Info',
                    'fields' => [
                        'website_name',
                        'alternate_name',
                    ],
                ],
                'tools'     => [
                    'title'  => 'Tools',
                    'fields' => [
                        'bing_webmaster_tools',
                        'google_search_console',
                        'yandex_webmaster_tools',
                    ],
                ],
                'security'  => [
                    'title'  => 'Security',
                    'fields' => [
                        'security_setting',
                    ],
                ],
            ],
        ],
    ];

    /**
     * The form actions.
     *
     * @var array
     */
    protected $actions = [
        'update' => [
            'redirect' => 'admin/seo_tool',
        ],
    ];

    /**
     * The form buttons
     *
     * @var array
     */
    protected $buttons = ['default'];

    /**
     * The form options.
     *
     * @var array
     */
    protected $options = [
        'breadcrumb' => false,
    ];

    /**
     * The form assets.
     *
     * @var array
     */
    protected $assets = [
        'styles.css' => [
            'module::scss/settings.scss',
        ],
        'scripts.js' => [
            'module::js/settings.js',
        ],
    ];

    /**
     * Fires on form ready
     */
    public function onReady()
    {
        $this->setFormMode('edit');
    }

    /**
     * Fires on post form
     *
     * @param SettingRepositoryInterface $settings The settings
     */
    public function onPost(SettingRepositoryInterface $settings)
    {
        $request = app('request')->all();

        foreach ($this->fields as $field)
        {
            if ($value = array_get($request, array_get($field, 'field')))
            {
                if (is_array($value))
                {
                    $value = serialize($value);
                }

                $settings->set(
                    array_get($field, 'bind'),
                    $value
                );
            }
        }
    }
}
