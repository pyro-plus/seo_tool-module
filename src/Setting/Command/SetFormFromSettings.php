<?php namespace Defr\SeoToolModule\Setting\Command;

use Anomaly\SettingsModule\Setting\Contract\SettingRepositoryInterface;

/**
 * Class SetFormFromSettings
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class SetFormFromSettings
{

    /**
     * Form builder
     *
     * @var SettingFormBuilder
     */
    protected $builder;

    /**
     * Create an instance of SetFormFromSettings class.
     *
     * @param $builder
     */
    public function __construct($builder)
    {
        $this->builder = $builder;
    }

    /**
     * Handle the command
     *
     * @return mixed
     */
    public function handle(SettingRepositoryInterface $settings)
    {
        $fields = $this->builder->getFields();

        $updatedFields = [];

        foreach ($fields as $slug => $field)
        {
            if ($setting = $settings->get(array_get($field, 'bind')))
            {
                $value = array_get($setting->getAttributes(), 'value');

                if (starts_with($value, 'included_'))
                {
                    $value = unserialize($value);

                    dd($value);
                }

                $field['value'] = array_get($setting->getAttributes(), 'value');
            }

            $updatedFields[$slug] = $field;
        }

        $this->builder->setFields($updatedFields);
    }
}
