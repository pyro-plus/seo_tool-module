<?php namespace Defr\SeoToolModule;

use Anomaly\Streams\Platform\Addon\Module\Module;

class SeoToolModule extends Module
{

    /**
     * The addon icon.
     *
     * @var string
     */
    protected $icon = 'fa fa-google';

    /**
     * The module sections.
     *
     * @var array
     */
    protected $sections = [
        'settings',
    ];
}
