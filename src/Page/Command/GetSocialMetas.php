<?php namespace Defr\SeoToolModule\Page\Command;

use Anomaly\PagesModule\Page\Command\GetPage;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Class GetSocialMetas
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class GetSocialMetas
{

    use DispatchesJobs;

    /**
     * @var array
     */
    protected $metas = [
        'meta_fb_title',
        'meta_fb_description',
        'meta_fb_image',
        'meta_tw_title',
        'meta_tw_description',
        'meta_tw_image',
    ];

    /**
     * Included metas
     *
     * @var null|string|array
     */
    protected $includes;

    /**
     * Create an instance of GetSocialMetas
     *
     * @param null|string|array $includes The includes
     */
    public function __construct($includes)
    {
        $this->includes = $includes;
    }

    /**
     * Return the form fields.
     *
     * @return Collection
     */
    public function handle()
    {
        if (is_null($this->includes))
        {
            return $this->getMetasFromPage($this->metas);
        }

        if (is_string($this->includes))
        {
            $this->includes = [$this->includes];
        }

        if (is_array($this->includes))
        {
            return $this->getMetasFromPage($this->includes);
        }
    }

    /**
     * Gets the metas from page.
     *
     * @param      array  $metas  The metas
     * @return     Collection  The metas from page.
     */
    public function getMetasFromPage($metas)
    {
        /* @var PageInterface $page */
        $page = $this->dispatch(new GetPage());

        $fields = collect([]);

        foreach ($metas as $meta)
        {
            if (is_string($meta))
            {
                $method = camel_case('get_' . $meta);

                $fields->put($meta, $page->$method());
            }
        }

        return $fields;
    }
}
