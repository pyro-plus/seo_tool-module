<?php namespace Defr\SeoToolModule\Page\Form;

use Anomaly\PagesModule\Page\Form\PageEntryFormBuilder;

/**
 * Class PageEntryFormSections
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class PageEntryFormSections extends \Anomaly\PagesModule\Page\Form\PageEntryFormSections
{

    /**
     * Return the form fields.
     *
     * @param PageEntryFormBuilder $builder
     */
    public function handle(PageEntryFormBuilder $builder)
    {
        parent::handle($builder);

        $sections = $builder->getSections();

        array_set(
            $sections,
            'page.tabs.reports',
            [
                'view'  => 'defr.module.seo_tool::form/reports_section',
                'title' => 'SEO Reports',
            ]
        );

        array_set(
            $sections,
            'page.tabs.seo.fields',
            array_merge(
                array_get($sections, 'page.tabs.seo.fields'),
                [
                    'page_meta_fb_title',
                    'page_meta_fb_description',
                    'page_meta_fb_image',
                    'page_meta_tw_title',
                    'page_meta_tw_description',
                    'page_meta_tw_image',
                ]
            )
        );

        $builder->setSections($sections);
    }
}
