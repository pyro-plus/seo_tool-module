<?php namespace Defr\SeoToolModule\Page\Form;

use Anomaly\PagesModule\Page\Form\PageFormBuilder;

/**
 * Class PageEntryFormSections
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class PageFormFields extends \Anomaly\PagesModule\Page\Form\PageFormFields
{

    /**
     * Return the form fields.
     *
     * @param PageFormBuilder $builder
     */
    public function handle(PageFormBuilder $builder)
    {
        parent::handle($builder);

        $builder->setFields(
            [
                '*',
                'serp' => [
                    'type'     => 'anomaly.field_type.textarea',
                    'disabled' => true,
                ],
            ]
        );
    }
}
