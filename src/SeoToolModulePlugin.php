<?php namespace Defr\SeoToolModule;

use Anomaly\Streams\Platform\Addon\Plugin\Plugin;
use Anomaly\Streams\Platform\Support\Decorator;
use Defr\SeoToolModule\Page\Command\GetSocialMetas;

/**
 * Class SeoToolModulePlugin
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class SeoToolModulePlugin extends Plugin
{

    /**
     * Get the plugin functions.
     *
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction(
                'social_metas',
                function ($include = null)
                {
                    return (new Decorator())
                        ->decorate(
                            $this->dispatch(new GetSocialMetas($include))
                        );
                }
            ),
        ];
    }
}
