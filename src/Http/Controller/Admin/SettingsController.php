<?php namespace Defr\SeoToolModule\Http\Controller\Admin;

use Anomaly\Streams\Platform\Http\Controller\AdminController;
use Defr\SeoToolModule\Setting\Command\SetFormFromSettings;
use Defr\SeoToolModule\Setting\Form\SettingFormBuilder;

/**
 * Class SettingsController
 *
 * @author        PyroCMS, Inc. <support@pyrocms.com>
 * @author        Denis Efremov <efremov.a.denis@gmail.com>
 *
 * @link          http://pyrocms.com/
 */
class SettingsController extends AdminController
{

    /**
     * Display an index of existing entries.
     *
     * @param   SettingFormBuilder $form
     * @return  Response
     */
    public function index(SettingFormBuilder $form)
    {
        $this->dispatch(new SetFormFromSettings($form));

        return $form->render();
    }
}
