<?php namespace Defr\SeoToolModule;

use Anomaly\PagesModule\Page\Form\PageEntryFormSections;
use Anomaly\PagesModule\Page\PageModel;
use Anomaly\Streams\Platform\Addon\AddonServiceProvider;

class SeoToolModuleServiceProvider extends AddonServiceProvider
{

    /**
     * The module routes
     *
     * @var array
     */
    protected $routes = [
        'admin/seo_tool' => 'Defr\SeoToolModule\Http\Controller\Admin\SettingsController@index',
    ];

    /**
     * The module bindings
     *
     * @var array
     */
    protected $bindings = [
        PageEntryFormSections::class => \Defr\SeoToolModule\Page\Form\PageEntryFormSections::class,
    ];

    /**
     * The module additional meta fields
     *
     * @var array
     */
    protected $metas = [
        'meta_fb_title',
        'meta_fb_description',
        'meta_fb_image',
        'meta_tw_title',
        'meta_tw_description',
        'meta_tw_image',
    ];

    /**
     * The module plugins
     *
     * @var array
     */
    protected $plugins = [
        SeoToolModulePlugin::class,
    ];

    /**
     * Register the addon
     *
     * @param PageModel $page The page
     */
    public function register(PageModel $page)
    {
        foreach ($this->metas as $slug)
        {
            $page->bind(
                'get_' . $slug,
                function () use ($slug)
                {
                    return $this->getFieldValue($slug);
                }
            );
        }

        $page->bind(
            'get_metadata',
            function ()
            {
                $metas = [];

                foreach ($this->getAssignmentFieldSlugs() as $slug)
                {
                    if (starts_with($slug, 'meta_'))
                    {
                        $metas[$slug] = $this->getFieldValue($slug);
                    }
                }

                return $metas;
            }
        );
    }
}
